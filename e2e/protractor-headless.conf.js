protractor = require('./protractor.conf.js');
protractor.config.multiCapabilities = [{
  browserName: 'chrome',
  chromeOptions: {
     args: [
      '--headless',
      '--disable-gpu',
      '--window-size=1366,720',
      'lang=es-ES'
    ]
  }
}];

exports.config = protractor.config;
