import { browser } from 'protractor';

describe('Protractor Demo App', function() {
  it('check title', async () => {
    browser.get(browser.baseUrl);

    let expected = "DemoProtractorAngular";
    let actual = browser.getTitle();

    expect(actual).toEqual(expected);

  });
});

