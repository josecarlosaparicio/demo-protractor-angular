# DemoProtractorAngular

Proyecto para probar Protractor junto con una aplicación en Angular.

## Instalación proyecto

``` bash
npm i
```

## Ejecución tests e2e

Arrancar automaticamente la aplicación angular y a continuación lanzar los tests e2e:

``` bash
npm run e2e
```

Lanzar únicamente los tests e2e:

``` bash
npm run e2e:standalone
```

Lanzar únicamente los tests e2e pero en modo chrome headless:

``` bash
npm run e2e:standalone:headless
```
